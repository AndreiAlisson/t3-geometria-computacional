CFLAGS = -Wall -g -o delaunay
CC = gcc

all: delaunay

delaunay: main.c
	$(CC) $(CFLAGS) main.c

clean:
	rm delaunay