/*Autor: Andrei Alisson GRR20163061*/

#include <stdio.h>
#include <stdlib.h>

/*Estrutura que representa um ponto*/
typedef struct Vertice{

	int coord_X;	//Coordenada X de um vertice
	int coord_Y;	//Coordenada Y de um vertice
	int index;		//Índice do vértice na lista

}Vertice;

/*Estrutura pra lista de vertices*/
typedef struct VerticeList{

	int num_Vertices;	//Número de vertices
	int acesso;			//Índice de acesso
	Vertice *vertices;	//Lista de vertices

}V_List;

/*Estrutura que representa um triângulo*/
typedef struct Triangulo{

	Vertice vertice_A, vertice_B, vertice_C;//Vértices do triângulo
	int index_A, index_B, index_C;			//Índices dos vértices do triângulo
	int viz_A, viz_B, viz_C;				//Índices dos triângulos vizinhos
	int eh_Valido;							//Verifica se o triângulo é válido
	int viz_control;						//Variável de controle
	int index;								//Índice FINAL do triângulo

}Triang;

/*Estrutura pra lista de triângulos*/
typedef struct TrianguloList{

	int num_Triangs;//Número de triângulos
	int index;		//Índice do triângulo
	Triang *triangs;//Triângulos

}T_List;

/*Estrutura que representa uma aresta (utilizada somente pro algoritmo bowyer watson)*/
typedef struct Poligono{

	Vertice vertice_A, vertice_B;//Aresta

}Poligono;

/*Estrutura pra lista de arestas*/
typedef struct PoligonoList{

	int num_Poligonos;	//Número de triângulos
	Poligono *poligonos;//Triângulos

}P_List;

/*void log_Triangulo(Triang t, int index){

	if(t.eh_Valido)
		fprintf(stderr, "t[%d]: (%d, %d) (%d, %d), (%d, %d)\n", index,
			t.vertice_A.coord_X, t.vertice_A.coord_Y,
			t.vertice_B.coord_X, t.vertice_B.coord_Y,
			t.vertice_C.coord_X, t.vertice_C.coord_Y);

}

void log_Poligono(Poligono p, int index){

	fprintf(stderr, "p[%d]: (%d, %d), (%d, %d)\n", index,
		p.vertice_A.coord_X, p.vertice_A.coord_Y,
		p.vertice_B.coord_X, p.vertice_B.coord_Y);

}*/

/*Imprime saída especificada*/
void imprime_Saida(T_List t, V_List v){

	printf("%d\n", v.num_Vertices);
	for(int i = 0; i < v.num_Vertices; i++)
		printf("%d %d\n", v.vertices[i].coord_X, v.vertices[i].coord_Y);

	printf("%d\n", t.num_Triangs);
	for(int i = 0; i < t.index; i++){
		if(t.triangs[i].eh_Valido)
			printf("%d %d %d %d %d %d\n", t.triangs[i].index_A, t.triangs[i].index_B, t.triangs[i].index_C,
										t.triangs[i].viz_A, t.triangs[i].viz_B, t.triangs[i].viz_C);
	}

}

/*Lê a entrada de dados*/
V_List le_Entrada(){

	V_List v;

	scanf("%d", &v.num_Vertices);

	v.vertices = malloc(v.num_Vertices * sizeof(Vertice));

	for(int i = 0; i < v.num_Vertices; i++){
		scanf("%d %d", &v.vertices[i].coord_X, &v.vertices[i].coord_Y);
		v.vertices[i].index = i+1;
	}

	return v;

}

void insere_Indices(T_List *t, V_List v){

	int count = 1;
	int control;

	for(int i = 0; i < t->index; i++){
		if(t->triangs[i].eh_Valido){
			control = 0;
			t->triangs[i].index = count++;
			for(int j = 0; j < v.num_Vertices && control < 3; j++){
				if(t->triangs[i].vertice_A.coord_X == v.vertices[j].coord_X && t->triangs[i].vertice_A.coord_Y == v.vertices[j].coord_Y){
					t->triangs[i].index_A = v.vertices[j].index;
					control++;
				}
				if(t->triangs[i].vertice_B.coord_X == v.vertices[j].coord_X && t->triangs[i].vertice_B.coord_Y == v.vertices[j].coord_Y){
					t->triangs[i].index_B = v.vertices[j].index;
					control++;
				}
				if(t->triangs[i].vertice_C.coord_X == v.vertices[j].coord_X && t->triangs[i].vertice_C.coord_Y == v.vertices[j].coord_Y){
					t->triangs[i].index_C = v.vertices[j].index;
					control++;
				}
			}
		}
	}

}

/*Obtem a lista de vizinhos dos triângulos*/
void get_Vizinhos(T_List *t){

	int v_inter = 0;

	for(int i = 0; i < t->index - 1; i++){
		if(t->triangs[i].eh_Valido){
			for(int j = i + 1; j < t->index; j++){

				if(t->triangs[j].eh_Valido){
					if(t->triangs[i].vertice_A.coord_X == t->triangs[j].vertice_A.coord_X &&
						t->triangs[i].vertice_A.coord_Y == t->triangs[j].vertice_A.coord_Y)
						v_inter++;
					else if(t->triangs[i].vertice_A.coord_X == t->triangs[j].vertice_B.coord_X &&
						t->triangs[i].vertice_A.coord_Y == t->triangs[j].vertice_B.coord_Y)
						v_inter++;
					else if(t->triangs[i].vertice_A.coord_X == t->triangs[j].vertice_C.coord_X &&
						t->triangs[i].vertice_A.coord_Y == t->triangs[j].vertice_C.coord_Y)
						v_inter++;

					if(t->triangs[i].vertice_B.coord_X == t->triangs[j].vertice_A.coord_X &&
						t->triangs[i].vertice_B.coord_Y == t->triangs[j].vertice_A.coord_Y)
						v_inter++;
					else if(t->triangs[i].vertice_B.coord_X == t->triangs[j].vertice_B.coord_X &&
						t->triangs[i].vertice_B.coord_Y == t->triangs[j].vertice_B.coord_Y)
						v_inter++;
					else if(t->triangs[i].vertice_B.coord_X == t->triangs[j].vertice_C.coord_X &&
						t->triangs[i].vertice_B.coord_Y == t->triangs[j].vertice_C.coord_Y)
						v_inter++;

					if(t->triangs[i].vertice_C.coord_X == t->triangs[j].vertice_A.coord_X &&
						t->triangs[i].vertice_C.coord_Y == t->triangs[j].vertice_A.coord_Y)
						v_inter++;
					else if(t->triangs[i].vertice_C.coord_X == t->triangs[j].vertice_B.coord_X &&
						t->triangs[i].vertice_C.coord_Y == t->triangs[j].vertice_B.coord_Y)
						v_inter++;
					else if(t->triangs[i].vertice_C.coord_X == t->triangs[j].vertice_C.coord_X &&
						t->triangs[i].vertice_C.coord_Y == t->triangs[j].vertice_C.coord_Y)
						v_inter++;

					if(v_inter == 2){

						if(t->triangs[i].viz_control == 0)
							t->triangs[i].viz_A = t->triangs[j].index;		
						else if(t->triangs[i].viz_control == 1)
							t->triangs[i].viz_B = t->triangs[j].index;	
						else if(t->triangs[i].viz_control == 2)
							t->triangs[i].viz_C = t->triangs[j].index;	

						if(t->triangs[j].viz_control == 0)					
							t->triangs[j].viz_A = t->triangs[i].index;	
						else if(t->triangs[j].viz_control == 1)				
							t->triangs[j].viz_B = t->triangs[i].index;
						else if(t->triangs[j].viz_control == 2)
							t->triangs[j].viz_C = t->triangs[i].index;

						t->triangs[i].viz_control++;
						t->triangs[j].viz_control++;
						
					}
				}

				v_inter = 0;

			}
		}

	}
}

/*Retorna um triângulo ordenado no sentido anti-horário*/
Triang ordena_Anti_Horario(Triang t){

	float S = (t.vertice_A.coord_X*t.vertice_B.coord_Y -
		t.vertice_A.coord_Y*t.vertice_B.coord_X +
		t.vertice_A.coord_Y*t.vertice_C.coord_X -
		t.vertice_A.coord_X*t.vertice_C.coord_Y +
		t.vertice_B.coord_X*t.vertice_C.coord_Y -
		t.vertice_B.coord_Y*t.vertice_C.coord_X )/2;

	if(S > 0)
		return t;

	Triang t1;
	t1.vertice_A = t.vertice_C;
	t1.vertice_B = t.vertice_B;
	t1.vertice_C = t.vertice_A;

	return t1;

}

/*Verifica se circuncírculo do triângulo t contém o ponto v*/
int verifica_Circuncirculo(Triang t1, Vertice v){

	Triang t = ordena_Anti_Horario(t1);

	int d1, d2, d3, d4, d5, d6;
	d1 = (t.vertice_A.coord_X - v.coord_X) *
		(t.vertice_B.coord_Y - v.coord_Y) *
		((t.vertice_C.coord_X*t.vertice_C.coord_X -2*t.vertice_C.coord_X*v.coord_X + v.coord_X*v.coord_X) +
		(t.vertice_C.coord_Y*t.vertice_C.coord_Y - 2*t.vertice_C.coord_Y*v.coord_Y + v.coord_Y*v.coord_Y));
	d2 = (t.vertice_A.coord_Y - v.coord_Y) *
		((t.vertice_B.coord_X*t.vertice_B.coord_X -2*t.vertice_B.coord_X*v.coord_X + v.coord_X*v.coord_X) +
		(t.vertice_B.coord_Y*t.vertice_B.coord_Y - 2*t.vertice_B.coord_Y*v.coord_Y + v.coord_Y*v.coord_Y)) *
		(t.vertice_C.coord_X - v.coord_X);
	d3 = ((t.vertice_A.coord_X*t.vertice_A.coord_X -2*t.vertice_A.coord_X*v.coord_X + v.coord_X*v.coord_X) +
		(t.vertice_A.coord_Y*t.vertice_A.coord_Y - 2*t.vertice_A.coord_Y*v.coord_Y + v.coord_Y*v.coord_Y)) *
		(t.vertice_B.coord_X - v.coord_X) *
		(t.vertice_C.coord_Y - v.coord_Y);
	d4 = ((t.vertice_A.coord_X*t.vertice_A.coord_X -2*t.vertice_A.coord_X*v.coord_X + v.coord_X*v.coord_X) +
		(t.vertice_A.coord_Y*t.vertice_A.coord_Y - 2*t.vertice_A.coord_Y*v.coord_Y + v.coord_Y*v.coord_Y)) *
		(t.vertice_B.coord_Y - v.coord_Y) *
		(t.vertice_C.coord_X - v.coord_X);
	d5 = ((t.vertice_B.coord_X*t.vertice_B.coord_X -2*t.vertice_B.coord_X*v.coord_X + v.coord_X*v.coord_X) +
		(t.vertice_B.coord_Y*t.vertice_B.coord_Y - 2*t.vertice_B.coord_Y*v.coord_Y + v.coord_Y*v.coord_Y)) *
		(t.vertice_C.coord_Y - v.coord_Y) *
		(t.vertice_A.coord_X - v.coord_X);
	d6 = ((t.vertice_C.coord_X*t.vertice_C.coord_X -2*t.vertice_C.coord_X*v.coord_X + v.coord_X*v.coord_X) +
		(t.vertice_C.coord_Y*t.vertice_C.coord_Y - 2*t.vertice_C.coord_Y*v.coord_Y + v.coord_Y*v.coord_Y)) *
		(t.vertice_A.coord_Y - v.coord_Y) *
		(t.vertice_B.coord_X - v.coord_X);

	if(d1+d2+d3-d4-d5-d6 > 0)
		return 1;

	return 0;
}

/*Cria um triângulo*/
Triang insere_Triangulo(int x1, int y1, int x2, int y2, int x3, int y3){

	Triang t;

	t.vertice_A.coord_X = x1;
	t.vertice_A.coord_Y = y1;
	t.vertice_B.coord_X = x2;
	t.vertice_B.coord_Y = y2;
	t.vertice_C.coord_X = x3;
	t.vertice_C.coord_Y = y3;
	t.eh_Valido = 1;
	t.viz_control = 0;
	t.viz_A = t.viz_B = t.viz_C = 0;

	return t;

}

/*Insere o super triângulo que contém todos os pontos no índice 0 de T_List*/
T_List cria_Super_Triangulo(V_List v){

	int max = 0;
	T_List t;
	t.triangs = malloc(sizeof(Triang));

	for(int i = 0; i < v.num_Vertices; i++){

		if(abs(v.vertices[i].coord_X) > max){
			max = abs(v.vertices[i].coord_X);
		}
		if(abs(v.vertices[i].coord_Y) > max){
			max = abs(v.vertices[i].coord_Y);
		}

	}

	t.index = 0;
	t.triangs[t.index++] = insere_Triangulo((-3) * max, (-3) * max, 3 * max, (-3) * max, 0, 3 * max);
	t.num_Triangs = 1;

	return t;

}

int compartilha_Aresta(Vertice a, Vertice b, int atual_Bad_Triangulo, T_List t, int *bad_Triangles, int num_Bad_Triangles){
	
	for(int i = 0; i < num_Bad_Triangles; i++){
		if(bad_Triangles[i] != atual_Bad_Triangulo){
			if(t.triangs[bad_Triangles[i]].vertice_A.coord_X == a.coord_X && t.triangs[bad_Triangles[i]].vertice_A.coord_Y == a.coord_Y){
				if(t.triangs[bad_Triangles[i]].vertice_B.coord_X == b.coord_X && t.triangs[bad_Triangles[i]].vertice_B.coord_Y == b.coord_Y){
					return 1;
				}
			}
			else if(t.triangs[bad_Triangles[i]].vertice_A.coord_X == b.coord_X && t.triangs[bad_Triangles[i]].vertice_A.coord_Y == b.coord_Y){
				if(t.triangs[bad_Triangles[i]].vertice_B.coord_X == a.coord_X && t.triangs[bad_Triangles[i]].vertice_B.coord_Y == a.coord_Y){
					return 1;
				}
			}
			if(t.triangs[bad_Triangles[i]].vertice_B.coord_X == a.coord_X && t.triangs[bad_Triangles[i]].vertice_B.coord_Y == a.coord_Y){
				if(t.triangs[bad_Triangles[i]].vertice_C.coord_X == b.coord_X && t.triangs[bad_Triangles[i]].vertice_C.coord_Y == b.coord_Y){
					return 1;
				}
			}
			else if(t.triangs[bad_Triangles[i]].vertice_B.coord_X == b.coord_X && t.triangs[bad_Triangles[i]].vertice_B.coord_Y == b.coord_Y){
				if(t.triangs[bad_Triangles[i]].vertice_C.coord_X == a.coord_X && t.triangs[bad_Triangles[i]].vertice_C.coord_Y == a.coord_Y){
					return 1;
				}
			}
			if(t.triangs[bad_Triangles[i]].vertice_C.coord_X == a.coord_X && t.triangs[bad_Triangles[i]].vertice_C.coord_Y == a.coord_Y){
				if(t.triangs[bad_Triangles[i]].vertice_A.coord_X == b.coord_X && t.triangs[bad_Triangles[i]].vertice_A.coord_Y == b.coord_Y){
					return 1;
				}
			}
			if(t.triangs[bad_Triangles[i]].vertice_C.coord_X == b.coord_X && t.triangs[bad_Triangles[i]].vertice_C.coord_Y == b.coord_Y){
				if(t.triangs[bad_Triangles[i]].vertice_A.coord_X == a.coord_X && t.triangs[bad_Triangles[i]].vertice_A.coord_Y == a.coord_Y){
					return 1;
				}
			}
		}
	}

	return 0;
}

Poligono insere_Poligono(Vertice a, Vertice b){

	Poligono p;
	p.vertice_A = a;
	p.vertice_B = b;

	return p;

}

P_List define_Poligono(T_List t, int *bad_Triangles, int num_Bad_Triangles){

	P_List pol;
	pol.poligonos = malloc(sizeof(Poligono));
	pol.num_Poligonos = 0;
	for(int j = 0; j < num_Bad_Triangles; j++){

		if(t.triangs[bad_Triangles[j]].eh_Valido){

			if(!compartilha_Aresta(
				t.triangs[bad_Triangles[j]].vertice_A,
				t.triangs[bad_Triangles[j]].vertice_B, bad_Triangles[j], t, bad_Triangles, num_Bad_Triangles
				)){
				pol.poligonos = realloc(pol.poligonos, ++pol.num_Poligonos * sizeof(Poligono));
				pol.poligonos[pol.num_Poligonos-1] = insere_Poligono(t.triangs[bad_Triangles[j]].vertice_A, t.triangs[bad_Triangles[j]].vertice_B);
			}
			if(!compartilha_Aresta(
				t.triangs[bad_Triangles[j]].vertice_B,
				t.triangs[bad_Triangles[j]].vertice_C, bad_Triangles[j], t, bad_Triangles, num_Bad_Triangles
				)){
				pol.poligonos = realloc(pol.poligonos, ++pol.num_Poligonos * sizeof(Poligono));
				pol.poligonos[pol.num_Poligonos-1] = insere_Poligono(t.triangs[bad_Triangles[j]].vertice_B, t.triangs[bad_Triangles[j]].vertice_C);
			}
			if(!compartilha_Aresta(
				t.triangs[bad_Triangles[j]].vertice_C,
				t.triangs[bad_Triangles[j]].vertice_A, bad_Triangles[j], t, bad_Triangles, num_Bad_Triangles
				)){
				pol.poligonos = realloc(pol.poligonos, ++pol.num_Poligonos * sizeof(Poligono));
				pol.poligonos[pol.num_Poligonos-1] = insere_Poligono(t.triangs[bad_Triangles[j]].vertice_C, t.triangs[bad_Triangles[j]].vertice_A);
			}
		}
	}

	return pol;

}

int tem_Super_Triangulo(Triang t, Triang super_Triangulo){

	if((t.vertice_A.coord_X == super_Triangulo.vertice_A.coord_X && t.vertice_A.coord_Y == super_Triangulo.vertice_A.coord_Y) ||
		(t.vertice_A.coord_X == super_Triangulo.vertice_B.coord_X && t.vertice_A.coord_Y == super_Triangulo.vertice_B.coord_Y)||
		(t.vertice_A.coord_X == super_Triangulo.vertice_C.coord_X && t.vertice_A.coord_Y == super_Triangulo.vertice_C.coord_Y)){
		return 1;
	}
	if((t.vertice_B.coord_X == super_Triangulo.vertice_A.coord_X && t.vertice_B.coord_Y == super_Triangulo.vertice_A.coord_Y) ||
		(t.vertice_B.coord_X == super_Triangulo.vertice_B.coord_X && t.vertice_B.coord_Y == super_Triangulo.vertice_B.coord_Y)||
		(t.vertice_B.coord_X == super_Triangulo.vertice_C.coord_X && t.vertice_B.coord_Y == super_Triangulo.vertice_C.coord_Y)){
		return 1;
	}
	if((t.vertice_C.coord_X == super_Triangulo.vertice_A.coord_X && t.vertice_B.coord_Y == super_Triangulo.vertice_A.coord_Y) ||
		(t.vertice_C.coord_X == super_Triangulo.vertice_B.coord_X && t.vertice_B.coord_Y == super_Triangulo.vertice_B.coord_Y)||
		(t.vertice_C.coord_X == super_Triangulo.vertice_C.coord_X && t.vertice_B.coord_Y == super_Triangulo.vertice_C.coord_Y)){
		return 1;
	}

	return 0;
}

/*Algoritmo de Bowyer Watson*/
T_List bowyer_Watson(V_List v){

	T_List t = cria_Super_Triangulo(v);
	int *bad_Triangles = malloc(sizeof(int));
	int num_Bad_Triangles = 0;
	P_List pol;

	/*itera sobre os pontos*/
	for(int i = 0; i < v.num_Vertices; i++){

		num_Bad_Triangles = 0;

		/*itera sobre os triângulos pra verificar os bad_Triangles do ponto v[i]*/
		for(int j = 0; j < t.index; j++){
			if(t.triangs[j].eh_Valido){
				if(verifica_Circuncirculo(t.triangs[j], v.vertices[i])){
					bad_Triangles = realloc(bad_Triangles, ++num_Bad_Triangles * sizeof(int));
					bad_Triangles[num_Bad_Triangles - 1] = j;
				}
			}
		}
		/*define os bad_Triangulos em pol*/
		pol = define_Poligono(t, bad_Triangles, num_Bad_Triangles);

		/*remove triângulos de bad_Triangles da estrutura de triângulos*/
		for(int j = 0; j < num_Bad_Triangles; j++){
			t.triangs[bad_Triangles[j]].eh_Valido = 0;
			t.num_Triangs--;
		}
		/*liga arestas de pol com o ponto v[i]*/
		for(int j = 0; j < pol.num_Poligonos; j++){
			t.triangs = realloc(t.triangs, ++t.index * sizeof(Triang));
			t.num_Triangs++;
			t.triangs[t.index-1] = insere_Triangulo(
									pol.poligonos[j].vertice_A.coord_X,
									pol.poligonos[j].vertice_A.coord_Y,
									pol.poligonos[j].vertice_B.coord_X,
									pol.poligonos[j].vertice_B.coord_Y,
									v.vertices[i].coord_X,
									v.vertices[i].coord_Y);
		}

	}
	/*Remove triângulos que contém vértices do super triângulo*/
	for(int i = 0; i < t.index; i++){
		if(t.triangs[i].eh_Valido){
			if(tem_Super_Triangulo(t.triangs[i], t.triangs[0])){
				t.triangs[i].eh_Valido = 0;
				t.num_Triangs--;
			}
		}
	}


	return t;

}

int main(){

	V_List v;
	T_List t;

	v = le_Entrada();

	t = bowyer_Watson(v);

	insere_Indices(&t, v);

	get_Vizinhos(&t);

	imprime_Saida(t, v);


}